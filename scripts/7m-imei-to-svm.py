#!/usr/bin/env python
"""
convert csv files into libsvm format
"""

import sys
import math

for line in sys.stdin:
    v = line.split(',')
    imei = v[0]
    out = "0" if len(imei) > 30 else imei
    numfeat = 0
    for i in xrange(1, len(v)):
        val = v[i].strip()
        if val is not None and len(val) > 0:
            try:
                x = float(val)
                val = math.log(1.0 + x) if len(v) - i > 200 else x
            except Exception as e:
                print >> sys.stderr, "val=" + val + "; x=" + str(x)
                print >> sys.stderr, str(e) + '  --  skip line: ' + line
                numfeat = 0
                break
            if val > 1e-6 or val < -1e-6:
                if numfeat == 0:
                    out += "\t" + str(i-1) + ':' + "%.8f"%(val)
                else:
                    out += " " + str(i-1) + ':' + "%.8f"%(val)
                numfeat = numfeat + 1
    if numfeat > 0:
        print out
    else:
        print >>sys.stderr, "missing line: imei=" + imei
