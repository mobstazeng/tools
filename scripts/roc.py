#!/usr/bin/env python
"""
Usage: roc.py --dat <label-pred-pair-file>  --pos pos-label --label label-col-index --score score-col-index
"""

import argparse
import sys
import sklearn
import numpy as np
from sklearn import metrics

def create_parser():
    """parse command line input"""
    parser = argparse.ArgumentParser(description='Pinnability Model and Evalution')
    parser.add_argument('--dat', '-i', required=True, help='result data in <label score> line format')
    parser.add_argument('--pos', default="1", '-p', help='positive label weights')
    parser.add_argument('--label', '-l', type=int, default=0, help='label column starts from 0.')
    parser.add_argument('--score', '-s', type=int, default=1, help='score column starts from 0.')
    return parser

def is_positive(y, positives):
    if len(positives) == 0:
        return y > 0
    return y in positives

if __name__ == "__main__":
  args = create_parser().parse_args()
  pos_labels = [int(x) for x in args.pos.split(',')]
  positives = dict(zip(pos_labels, pos_labels))
  print >>sys.stderr, positives

  lbl_idx = int(args.label)
  dat = np.loadtxt(args.dat)
  # filter the labels that are > 0 and not in positives
  if len(positives) > 0:
    dat = np.array(filter(lambda t: (t[lbl_idx] <= 0 or int(t[lbl_idx]) in positives), dat))
  print >>sys.stderr, "loaded data shape:", dat.shape

  y = np.array(dat[:,int(args.label)])
  for i in xrange(len(y)):
    if y[i] > 0:
        y[i] = 1
    else:
        y[i] = 0

  scores = np.array(dat[:,args.score])
  scores[y < 0.5] += 0.00001 # breaks the ties when the model output same scores
  fpr, tpr, thresholds = metrics.roc_curve(y, scores, pos_label=1.0)
  print "roc score: ", metrics.auc(fpr, tpr)
  # print "thresholds: ", str(thresholds)
  # print "fpr: ", str(fpr)
  # print "tpr: ", str(tpr)
  file = args.dat.split('/')[-1]
  with open(file + ".roc", "w") as f:
    f.write("\n".join([" ".join([str(v[0]), str(v[1])]) for v in zip(fpr, tpr)]))

  prec, recall, thresholds = metrics.precision_recall_curve(y, scores, pos_label=1.0)
  print "precision score: ", metrics.average_precision_score(y, scores)
  # print "thresholds: ", str(thresholds)
  # print "prec: ", str(prec)
  # print "recall: ", str(recall)

  with open(args.dat.split('/')[-1] + ".pr", "w") as f:
    f.write("\n".join([" ".join([str(v[0]), str(v[1])]) for v in zip(prec, recall)]))
