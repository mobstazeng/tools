#!/usr/bin/env python

"""
convert csv to svm format. using log(1+x) for normalizing.
"""

import sys
import math

for line in sys.stdin.readlines():
    v = line.split(',')
#   imei flag
    out = v[0]
    numfeat = 0
    for i in xrange(1, len(v)):
        val = v[i].strip()
        if val is not None and len(val) > 0:
            try:
                val = math.log(1.0 + float(val))
            except:
                print >> sys.stderr, 'skip line: ' + line
                numfeat = 0
                break
            if abs(val) > 1e-6:
                if numfeat == 0:
                    out += "\t" + str(i-1) + ':' + "%.8f"%(val)
                else:
                    out += " " + str(i-1) + ':' + "%.8f"%(val)
                numfeat = numfeat + 1

    if numfeat > 0: print out
